import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Udon {
    private JPanel root;
    private JButton SPICYCURRY;
    private JButton Salad;
    private JButton Lassi;
    private JTextArea textArea1;
    private JButton PORKCURRY;
    private JButton BEEFCURRY;
    private JButton HASHEDBEEF;
    private JTextArea textArea2;
    private JButton Checkout;
    private JLabel total;
    private int sum = 0;



    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            int where = JOptionPane.showConfirmDialog(
                    null,
                    "Will you be dining in?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if (where == 0) {/*in*/
                double price2;
                price2 = price * 1.1;
                sum += price2;

                textArea1.setText("Order for " + food + " received.");
                JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
                String currentText = textArea2.getText();
                textArea2.setText(currentText + food +" "+ (int)price2+ "yen\t In\n");
                total.setText(sum + " yen");
            }
            if(where==1) {
                double price2;
                price2 = price * 1.08;
                sum += price2;


                textArea1.setText("Order for " + food + " received.");
                JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
                String currentText = textArea2.getText();
                textArea2.setText(currentText + food +" "+ (int)price2+ "yen\t To\n");
                total.setText(sum + " yen");
            }
        }if(confirmation == 1){
        }
    }

    public Udon() {
        total.setText("0yen");

        PORKCURRY.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                order("PORKCURRY",591);
            }
        });
        PORKCURRY.setIcon(new ImageIcon(
                this.getClass().getResource("Pork-001.png")
        ));

        BEEFCURRY.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("BEEFCURRY", 654);
            }
        });
        BEEFCURRY.setIcon(new ImageIcon(
                this.getClass().getResource("Beef-003.png")
        ));

        SPICYCURRY.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("SPICYCURRY", 891);
            }
        });
        SPICYCURRY.setIcon(new ImageIcon(
                this.getClass().getResource("spicy.png")
        ));

        HASHEDBEEF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("HASHEDBEEF", 653);
            }
        });
        HASHEDBEEF.setIcon(new ImageIcon(
                this.getClass().getResource("Hashed beef-004.png")
        ));

        Salad.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Green Salad", 127);
            }
        });
        Salad.setIcon(new ImageIcon(
                this.getClass().getResource("salads-002.jpg")
        ));

        Lassi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Lassi (plain)", 182);
            }
        });
        Lassi.setIcon(new ImageIcon(
                this.getClass().getResource("Lassi.png")
        ));
        Checkout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    textArea1.setText("Order for " + sum + " received.");
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + sum + "yen. It will be served as soon as possible.");
                    sum = 0;
                    total.setText(sum+" yen");
                    String currentText=null;
                    textArea2.setText(currentText);
                    String food=null;
                    textArea1.setText(food);

                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Udon");
        frame.setContentPane(new Udon().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
